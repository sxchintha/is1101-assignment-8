#include <stdio.h>

struct Student
{
	char fName[20];
	char subject[20];
	int marks;
	
};

// print students' details
void printStudents(struct Student std[], int stdCount)
{
	printf("\n---Students List---\n");
	for(int i=0; i<stdCount; i++)
	{
		printf("Student name: %s  Subject: %s  Marks: %d\n", std[i].fName, std[i].subject, std[i].marks);
	}

}

int main()
{
	int stdCount;

	while(1)
	{
		printf("Please enter the number of Students: ");
		scanf("%d", &stdCount);

		if(stdCount<5)
			printf("Minimum number of Students is 5.\n");
		else
			break;
	}

	struct Student std[stdCount];
	char sName[20], sSub[20];

	for(int i=0; i<stdCount; i++)
	{
		printf("\nStudent's First Name: ");
		scanf("%s", std[i].fName);

		printf("Subject: ");
		scanf("%s", std[i].subject);

		printf("Marks: ");
		scanf("%d", &std[i].marks);
	}

	printStudents(std, stdCount);


	return 0;
}
